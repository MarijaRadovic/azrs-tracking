# AZRS tracking

### Projekat u okviru kursa Alati za razvoj softvera na Matematičkom fakultetu. Alati se primenjuju na projekat 
[Stratego](https://gitlab.com/MarijaRadovic/10-stratego) koji je razvijen u okviru kursa Razvoj softvera. [Link](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/10-stratego) do grupnog projekta.

## Alati
- GDB
- CMake
- Valgrind
- Gamma Ray
- Clang Tidy
- Clang Format
- Clang Analyser
- Docker
- GCov